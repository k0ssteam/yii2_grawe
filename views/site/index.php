<?php

/* @var $this yii\web\View */
//$csrfTokenName = Yii::$app->request->csrfTokenName;
//echo $csrfTokenName;
/*
echo Yii :: $app->getRequest()->csrfParam;
echo "<hr>";
echo  Yii :: $app->getRequest()->getCsrfToken();
echo "<hr>";*/

$this->title = 'On_line Insurance';
$this->registerJsFile('../vue/test/dist/build.js',  ['position' => yii\web\View::POS_END]);
?>

<div class="row-full">
    <section class="Hero Hero--bg Hero--image" id="travelInsurance">
        <div class="Hero-inner">
            <div class="Hero-column">
                <div class="Hero-intro">
                    <div class="Hero-media" style="background-image: url('https://cdn.worldnomads.net/Media/Default/TravelInsuranceImages/PageHero/wn-TI-home-traveller-hero.jpg')"></div>
                    <ol id="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList" class="Hero-breadcrumb BreadCrumb BreadCrumb--reverse">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/">
                                <span itemprop="name">World Nomads</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/travel-insurance/" class="current">
                                <span itemprop="name">Travel Insurance</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>
                    </ol>

                    <h1 class="Hero-title Hero-copy CopyBreak CopyBreak--reverse CopyBreak--centered CopyBreak--thick">
Travel Insurance                </h1>
                </div>
                <div class="Hero-content">
                    <p class="Hero-standfirst Hero-copy standfirst">Simple and Flexible Travel Insurance.  Buy at home or while traveling, and claim online from anywhere in the world.</p>
                    <div data-smoothscroll="">
                        <a href="#insuranceQuotePanel" class="Hero-cta button primary large arrow">Get a Price</a>
                    </div>

                </div>
            </div>
        </div></section>
</div>


<div id="app"></div>


